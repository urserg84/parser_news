<?xml version="1.0"?>

<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
  <html>
    <head>
        <link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css"></link>
        <link rel="stylesheet" type="text/css" href="/css/style.css"></link>
        <title>Новости</title>
    </head>
    <body>
        <h2>Новости</h2>
        <table border="1">
        <tr bgcolor="#9acd32">
            <th>Дата</th>
            <th>Заголовок</th>
            <th>Сообщение</th>
            <th>Смотреть</th>
        </tr>
        <xsl:for-each select="news/new" >
            <tr>
            <td><xsl:value-of select="date"/></td>
            <td><xsl:value-of select="header"/></td>
            <td><xsl:value-of select="body"/></td>
            <td><xsl:copy-of select="link/node()" /></td>
            </tr>
        </xsl:for-each>
        </table>
    </body>
  </html>
</xsl:template>

</xsl:stylesheet>