@footer[]
</div>
<footer class="footer">
    $now[^date::now[]]
    <div class="footer-copyright text-center py-3">© $now.year Copyright</div>
</footer>
</body>
</html>